package praktikum12;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

/**
 * Ringjoone valemi jÃ¤rgi ringi joonistamise nÃ¤ide
 * @author Mikk Mangus
 */
@SuppressWarnings("serial")
public class spiraal extends Applet {

    private Graphics g;
    
    public void paint(Graphics g) {
        this.g = g;
        joonistaTaust();
        joonistaRing();
    }
    
    /**
     * Katab tausta valgega
     */
    public void joonistaTaust() {
        int w = getWidth();
        int h = getHeight();
        g.setColor(Color.white);
        g.fillRect(0, 0, w, h);
    }
    
    /**
     * Joonistab ringi
     */
    public void joonistaRing() {
        g.setColor(Color.black);
        int keskkohtX = getWidth() / 2;
        int keskkohtY = getHeight() / 2;
       
        Polygon spiraaal = new Polygon();
        
        
        
        for (double nurk = 0; nurk <= Math.PI * 20; nurk = nurk + 1.57) {
        	 double raadius = 10+nurk*2;
            int x = (int) (raadius * Math.cos(nurk)+keskkohtX);
            int y = (int) (raadius * Math.sin(nurk)+keskkohtY);
            //g.fillRect(keskkohtX + x, keskkohtY + y, 2, 2);
            spiraaal.addPoint(x, y);
        }
        
        g.drawPolygon(spiraaal);
    }
}