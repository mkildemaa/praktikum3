package praktikum12;

import java.applet.Applet;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Timer;
import java.util.TimerTask;

public class Naidis extends Applet {
	BufferedImage image = new BufferedImage(500, 500, BufferedImage.TYPE_INT_ARGB);

	public void init() {
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				Naidis.this.repaint();
			}
		}, 100, 100);
		Graphics graphics = image.getGraphics();
		
	}

	public void paint(Graphics g) {
		g.drawImage(image, 0, 0, null);

		// Kysime kui suur aken on?
		int w = getWidth();
		int h = getHeight();

		g.setColor(Color.RED);

		Polygon p = new Polygon();

		p.addPoint(0, 0);
		p.addPoint(10 + (int) (Math.random() * 500), 10 + (int) (Math.random() * 700));
		p.addPoint(10 + (int) (Math.random() * 500), 10 + (int) (Math.random() * 700));
		p.addPoint(10 + (int) (Math.random() * 500), 10 + (int) (Math.random() * 700));
		p.addPoint(10 + (int) (Math.random() * 500), 10 + (int) (Math.random() * 700));
		p.addPoint(10 + (int) (Math.random() * 500), 10 + (int) (Math.random() * 700));
		g.fillPolygon(p);

	}
}
