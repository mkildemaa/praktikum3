package praktikum10;

import java.util.Arrays;

public class SuurimMassiiv {

	
	
	
	public static void main(String[] args) {
		
		
		int[][] neo = {
			    {1, 3, 6, 7},
			    {2, 3, 3, 1},
			    {17, 4, 5, 0},
			    {-20, 13, 16, 17}
			};
		
		int[] suurimad = new int[neo.length];
		for (int i = 0; i < neo.length; i++) {
			suurimad[i] = getSuurim(neo[i]);
		}
	
		
		
		
		System.out.println(Arrays.toString(suurimad));
	}

	
	
	
	
	private static int getSuurim(int[] massiiv) {
		
		int suurim = Integer.MIN_VALUE;
		for (int i = 0; i < massiiv.length; i++) {
			if (suurim < massiiv[i]) {
				suurim = massiiv[i];
				
			}
			
					}
		return suurim;
		
		
	}
	
	
}
	
	
	
	
	
	
	

