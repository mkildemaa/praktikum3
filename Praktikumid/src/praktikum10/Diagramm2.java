package praktikum10;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;


public class Diagramm2 {

	public static void main(String[] args) {
		

		Scanner sc = new Scanner(System.in);
		ArrayList<Integer> numbrid = new ArrayList<>();
		int suurim = Integer.MIN_VALUE;
		
		while (true) {
			System.out.println("Sisesta number");
			int input = sc.nextInt();
			
			
			if (input==0) {
				break;
			}
			
			if (suurim < input) {
				suurim = input;
			}
						
			numbrid.add(input);
		}
		
		double kordaja = 10.0 / suurim;
		String[] X = new String[numbrid.size()];
		
	
		for (int i = 0; i<numbrid.size(); i++) {
			X[i] = genereeriX(numbrid.get(i), kordaja);
			
			
		}
		for (int j = 9; j >= 0; j--) {
	for (int i = 0; i < X.length; i++) {
		if (X[i].length() > j) {
			System.out.printf("%2s ", X[i].charAt(j));
		}
	}
System.out.println();
}
		
		System.out.println("----------");
		for (int i = 0; i < numbrid.size(); i++) {
			System.out.printf("%2d ", numbrid.get(i));
		}
	}
	
	

	private static String genereeriX(int nr, double kordaja) {
		String X = "";
		for (int i = 0; i < nr * kordaja; i++) {
			X = X + "X";
		}
		return X;
	}


}
