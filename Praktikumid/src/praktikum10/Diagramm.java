package praktikum10;

import java.util.ArrayList;
import java.util.Scanner;


public class Diagramm {

	public static void main(String[] args) {
		

		Scanner sc = new Scanner(System.in);
		ArrayList<Integer> numbrid = new ArrayList<>();
		int suurim = Integer.MIN_VALUE;
		
		while (true) {
			System.out.println("Sisesta number");
			int input = sc.nextInt();
			
			
			if (input==0) {
				break;
			}
			
			if (suurim < input) {
				suurim = input;
			}
						
			numbrid.add(input);
		}
		
		double kordaja = 80.0 / suurim;
		for (int i = 0; i<numbrid.size(); i++) {
			String X = genereeriX(numbrid.get(i), kordaja);
			System.out.printf("%4d %s\n", numbrid.get(i), X);
		}
		

	}

	private static String genereeriX(int nr, double kordaja) {
		String X = "";
		for (int i = 0; i < nr * kordaja; i++) {
			X = X + "X";
		}
		return X;
	}


}
