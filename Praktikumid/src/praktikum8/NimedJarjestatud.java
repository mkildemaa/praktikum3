package praktikum8;

import java.util.ArrayList;
import java.util.Collections;

public class NimedJarjestatud {

	public static void main(String[] args) {

		ArrayList<String> nimed = new ArrayList<String>();
		System.out.println("Sisesta nimed! Tühi sisestud lõpetab.");

		while (true) {
			String nimi = TextIO.getlnString();
			if (nimi.equals("")) {
				System.out.println("Tähestikulises järjekorras:");
				break;
			} else {
				nimed.add(nimi);
			}
		}

		Collections.sort(nimed);
		for (String nimi : nimed) {
			System.out.println(nimi);
		}
	}
}
