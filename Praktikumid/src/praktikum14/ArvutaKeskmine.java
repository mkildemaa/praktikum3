package praktikum14;

import java.util.ArrayList;

public class ArvutaKeskmine {

	public static void main(String[] args) {

		System.out.println(arvutaKeskmine(teeNumbriteks("Kala.txt")));

	}

	public static ArrayList<Double> teeNumbriteks(String failiNimi) {
		ArrayList<String> read = Faililugeja.loeFail(failiNimi);
		ArrayList<Double> arrayNumbrid = new ArrayList<Double>();

		for (String line : read) {
			try {
				double nr = Double.parseDouble(line);
				arrayNumbrid.add(nr);

			} catch (NumberFormatException e) {
				System.out.println("Failis ei ole kõik numbrid nt: " + line);
			}
		}
		return arrayNumbrid;
	}

	public static double arvutaKeskmine(ArrayList<Double> arrayListike) {
		double a = 0;
		double b = 0;
		double vastus;
		for (Double rida : arrayListike) {
			a = a + rida;
			b++;
		}
		vastus = a / b;
		return vastus;

	}
}
