package praktikum11;

public class Astendaja {

	public static void main(String[] args) {

		System.out.println(astenda(2, 4));
		System.out.println(fibonacci(5));

	}

	public static int astenda(int a, int b) {

		if (b == 1) {
			return a;
		}

		return a * astenda(a, b - 1);
	}

	public static int fibonacci(int n) {

		if (n <= 1) {
			return n;
		} else {
			return fibonacci(n - 1) + fibonacci(n - 2);
		}

	}

}
