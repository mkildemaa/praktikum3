package praktikum4;

import lib.TextIO;

public class paroolicheck {
	public static void main(String[] args) {

		String parool = "kasepuu";
		String sisestatud = null;

		while (sisestatud != parool) {

			System.out.println("Sisesta parool: ");
			sisestatud = TextIO.getlnString();

			if (sisestatud.equals(parool)) {
				System.out.println("Õige");
				break;
			} else {
				System.out.println("Vale parool");
			}

		}

	}
}
