package praktikum13;

import praktikum13.TextIO;

public class Massiivid {

	public static void main(String[] args) {

		int[] arvud = { 3, 4, 6, -3, 9 };

		int[][] neo = { { 1, 1, 1, 1, 1 },
						{ 2, 3, 4, 5, 6 },
						{ 3, 4, 5, 6, 7 },
						{ 4, 5, 6, 7, 8 },
						{ 5, 6, 7, 8, 9 }, };

		Integer reanr;
		System.out.print("Mitmes rida? ");
		reanr = TextIO.getlnInt();
		reanr -= 1;
		
		System.out.println("Rea esimene nr;");
System.out.println(reaMiinimum(neo[reanr]));
		
		System.out.println("Rea viimane nr:");
		System.out.println(reaMaksimum(neo[reanr]));
		
				
	System.out.println("Rea summa:");
		System.out.println(reaSumma(neo[reanr]));
		
		System.out.println("kõikide ridade summad:");
		tryki(ridadeSummad(neo));
		
		System.out.println("Pea diagonaali summa");
		System.out.println(peaDiagonaaliSumma(neo));
		
		System.out.println("Kõrval diagonaali summa");
		System.out.println(korvalDiagonaaliSumma(neo));
		
		System.out.println("Viimaste numbrite summa");
		System.out.println(reaSumma(ridadeMaksimumid(neo)));
		
		System.out.println("Esimeste numbrite summa");
		System.out.println(reaSumma(ridadeMiinimumid(neo)));
		
	
		
		
		
	}

	/*
	 * Kirjutada meetod, mis trükib ekraanile ühel real parameetrina etteantud
	 * ühemõõtmelise täisarvumassiivi elemendid
	 */
	public static void tryki(int[] massiiv) {
		for (int arv : massiiv) {
			System.out.print(arv + " ");
		}
		System.out.println();
	}

	/*
	 * Kirjutada meetod, mis trükib ekraanile parameetrina etteantud
	 * kahemõõtmelise massiivi (maatriksi)
	 * 
	 * Kasuta maatriksi rea trükkimiseks kindlasti eelnevalt kirjutatud
	 * meetodit!
	 */
	public static void tryki(int[][] maatriks) {
		for (int[] rida : maatriks) {
			tryki(rida);
		}
	}

	/*
	 * Arvutada maatriksi iga rea elementide summa
	 */
	public static int[] ridadeSummad(int[][] maatriks) {
		int[] summad = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {
			summad[i] = reaSumma(maatriks[i]);
		}
		return summad;
	}

	public static int reaSumma(int[] massiiv) {
		int summa = 0;
		for (int arv : massiiv) {
			summa += arv;
		}
		return summa;
	}

	/*
	 * Arvutada kõrvaldiagonaali elementide summa (kõrvaldiagonaal on see, mis
	 * jookseb ülevalt paremast nurgast alla vasakusse nurka)
	 */
	public static int korvalDiagonaaliSumma(int[][] maatriks) {
		int summa = 0;
		for (int i = 0; i < maatriks.length; i++) {
			summa += maatriks[i][maatriks[i].length - i - 1];
		}
		return summa;
	}

	public static int peaDiagonaaliSumma(int[][] maatriks) {
		int summa = 0;
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				if (i == j) {
					summa += maatriks[i][j];
				}
				// System.out.print(maatriks[i][j] + "(i=" + i + " j=" + j + ")
				// ");
			}
			// System.out.println();
		}
		return summa;
	}

	public static int peaDiagonaaliSummaEfektiivsemalt(int[][] maatriks) {
		int summa = 0;
		for (int i = 0; i < maatriks.length; i++) {
			summa += maatriks[i][i];
		}
		return summa;
	}

	/*
	 * Leida iga rea suurim element
	 */
	public static int[] ridadeMaksimumid(int[][] maatriks) {
		int[] maksimumid = new int[maatriks.length];
		for (int i = 0; i < maksimumid.length; i++) {
			maksimumid[i] = reaMaksimum(maatriks[i]);
		}
		return maksimumid;
	}

	public static int reaMaksimum(int[] massiiv) {
		int maksimum = Integer.MIN_VALUE;
		for (int arv : massiiv) {
			if (arv > maksimum) {
				maksimum = arv;
			}
		}
		return maksimum;
	}
	
	public static int reaMiinimum(int[] massiiv) {
		int miinimum = Integer.MAX_VALUE;
		for (int arv : massiiv) {
			if (arv < miinimum) {
				miinimum = arv;
			}
		}
		return miinimum;
	}
	 
		public static int[] ridadeMiinimumid(int[][] maatriks) {
			int[] miinimumid = new int[maatriks.length];
			for (int i = 0; i < miinimumid.length; i++) {
				miinimumid[i] = reaMiinimum(maatriks[i]);
			}
			return miinimumid;
		}
	
}
